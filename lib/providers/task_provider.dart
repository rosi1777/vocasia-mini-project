import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:vocasia_mini_project/api/api_service.dart';
import 'package:vocasia_mini_project/api/base_url.dart';
import 'package:vocasia_mini_project/network/network_url_const.dart';
import 'package:vocasia_mini_project/utils/result_state.dart';
import '../models/task.dart';
import 'package:http/http.dart' as http;

class TaskProvider extends ChangeNotifier {
  final ApiService apiService = ApiService(baseUrl: url);

  late ResultState _state = ResultState.done;

  ResultState get state => _state;

  Future<List<Task>>? getTodayTask(String token) async {
    var response =
        await http.get(Uri.parse('${NetworkUrlConst.task}/today'), headers: {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    });
    if (response.statusCode == 210 || response.statusCode == 200) {
      var datas = jsonDecode(response.body);
      List taskList = datas['messages']['list'];
      List<Task> tasks = taskList.map((item) {
        return Task.fromJson(item);
      }).toList();
      return tasks;
    } else {
      return <Task>[];
    }
  }

  Future<List<Task>>? getAllTask(String token) async {
    var response = await http.get(Uri.parse(NetworkUrlConst.task), headers: {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    });
    print('Response Code = ${response.statusCode}');
    if (response.statusCode == 210 || response.statusCode == 200) {
      var datas = jsonDecode(response.body);
      List taskList = datas['messages']['list'];
      List<Task> tasks = taskList.map((item) {
        return Task.fromJson(item);
      }).toList();
      return tasks;
    } else {
      return <Task>[];
    }
  }

  markAsDone(String id, String token) async {
    await http.put(
        Uri.parse('https://apitodolist.nouky.xyz/todolist/markasdone/$id'),
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
  }

  Future getCounter(String token) async {
    var response = await http.get(Uri.parse('${NetworkUrlConst.task}/stats'),
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    if (response.statusCode == 200) {
      var datas = jsonDecode(response.body);
      var counter = datas['messages'];
      return counter;
    } else {
      return {};
    }
  }

  Future<dynamic> addTask(String token, String tittle, String description,
      String time, BuildContext context) async {
    try {
      _state = ResultState.loading;
      notifyListeners();

      await apiService.addTask(
          token: token, tittle: tittle, description: description, time: time);

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('New task added successfully'),
        ),
      );
      _state = ResultState.done;
      notifyListeners();
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Add task failed'),
        ),
      );
      _state = ResultState.error;
    }
  }

  Future<dynamic> updateTask(String token, String id, String tittle,
      String description, String time, BuildContext context) async {
    try {
      _state = ResultState.loading;
      notifyListeners();

      await apiService.updateTask(
          token: token,
          id: id,
          tittle: tittle,
          description: description,
          time: time);

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Update task successfully'),
        ),
      );
      _state = ResultState.done;
      notifyListeners();
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Update task failed'),
        ),
      );
      _state = ResultState.error;
    }
  }
}
