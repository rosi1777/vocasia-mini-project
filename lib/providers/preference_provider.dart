import 'package:flutter/foundation.dart';
import 'package:vocasia_mini_project/helper/preference_helper.dart';

class PreferenceProvider extends ChangeNotifier {
  PreferencesHelper preferencesHelper;

  PreferenceProvider({required this.preferencesHelper}) {
    getOnBoardingPreference();
    getTokenPreference();
  }

  bool _isOnBoardingViewed = false;
  bool get isOnBoardingViewed => _isOnBoardingViewed;
  String token = "";

  Future getOnBoardingPreference() async {
    _isOnBoardingViewed = await preferencesHelper.isOnBoardingViewed;
    notifyListeners();
  }

  Future getTokenPreference() async {
    token = (await preferencesHelper.token)!;
    notifyListeners();
  }

  void setOnBoardingPreference(bool value) async {
    preferencesHelper.setOnBoardingPreference(value);
    getOnBoardingPreference();
  }
}
