import 'package:flutter/material.dart';
import 'package:vocasia_mini_project/models/task.dart';
import 'package:vocasia_mini_project/providers/task_provider.dart';
import 'package:vocasia_mini_project/screen/add_task_screen.dart';
import 'package:vocasia_mini_project/screen/register_loading_screen.dart';
import 'package:vocasia_mini_project/screen/login_screen.dart';
import 'package:vocasia_mini_project/helper/preference_helper.dart';
import 'package:vocasia_mini_project/providers/preference_provider.dart';
import 'package:vocasia_mini_project/screen/onboarding_screen.dart';
import 'package:vocasia_mini_project/screen/register_screen.dart';
import 'package:vocasia_mini_project/screen/splash_screen.dart';
import 'package:vocasia_mini_project/screen/tabs_screen.dart';
import 'package:vocasia_mini_project/screen/task_detail_screen.dart';
import 'package:vocasia_mini_project/screen/update_task_screen.dart';
import 'package:vocasia_mini_project/theme.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
            create: (_) => PreferenceProvider(
                preferencesHelper: PreferencesHelper(
                    sharedPreferences: SharedPreferences.getInstance()))),
        ChangeNotifierProvider(create: (_) => TaskProvider())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: SplashScreen.routeName,
        theme: ThemeData(colorScheme: ColorScheme.light(primary: red)),
        routes: {
          LoginScreen.routeName: (context) => const LoginScreen(),
          OnBoardingScreen.routeName: (context) => const OnBoardingScreen(),
          SplashScreen.routeName: (context) => const SplashScreen(),
          RegisterScreen.routeName: (context) => const RegisterScreen(),
          LoadingRegisterScreen.routeName: (context) =>
              const LoadingRegisterScreen(),
          AddTaskScreen.routeName: (context) => AddTaskScreen(
                token: (ModalRoute.of(context)?.settings.arguments as String),
                signOut: (ModalRoute.of(context)?.settings.arguments
                    as VoidCallback),
              ),
          UpdateTaskScreen.routeName: (context) => UpdateTaskScreen(
                task: (ModalRoute.of(context)?.settings.arguments as Task),
              ),
          TaskDetailScreen.routeName: (context) => TaskDetailScreen(
                task: (ModalRoute.of(context)?.settings.arguments as Task),
              ),
          TabsScreen.routeName: (context) => TabsScreen(
                signOut: (ModalRoute.of(context)?.settings.arguments
                    as VoidCallback),
              )
        },
      ),
    );
  }
}
