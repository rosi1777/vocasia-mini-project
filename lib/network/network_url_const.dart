class NetworkUrlConst {
  static String login = "https://apitodolist.nouky.xyz/login";

  static String register = "https://apitodolist.nouky.xyz/register";

  static String delete = "https://apitodolist.nouky.xyz/todolist/delete/";

  static String update = "https://apitodolist.nouky.xyz/todolist/update/";

  static String create = "https://apitodolist.nouky.xyz/todolist/create";

  static String markAsDone =
      "https://apitodolist.nouky.xyz/todolist/markasdone/";

  static String task = "https://apitodolist.nouky.xyz/todolist";
}
