import 'package:flutter/material.dart';

Color red = const Color(0xffF1555A);
Color grey = const Color(0xffB4B4B4);
Color lightGrey = const Color(0xffEDEDED);
Color darkGrey = const Color(0xff807E7E);
Color green = const Color(0xff16D034);

List<Map<String, Object>> badgeColors = [
  {
    'id': 'todo',
    'colors': const [
      Color(0xffC83338),
      Color(0xffDE565B),
      Color(0xffD45459),
      Color(0xffDE565B),
    ]
  },
  {
    'id': 'overdue',
    'colors': const [
      Color(0xffC9B441),
      Color(0xffD6BB2F),
      Color(0xffC9B441),
    ]
  },
  {
    'id': 'done',
    'colors': const [
      Color(0xff30C047),
      Color(0xff30C047),
      Color(0xff3FB052),
      Color(0xff30C047),
    ]
  },
];

TextStyle font = const TextStyle(fontFamily: 'SfProDisplay');
