import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:vocasia_mini_project/models/task.dart';
import 'package:vocasia_mini_project/theme.dart';

class TaskDetailScreen extends StatelessWidget {
  final Task task;
  static const routeName = '/task_detail_screen';
  const TaskDetailScreen({Key? key, required this.task}) : super(key: key);

  String getDate() {
    return DateFormat('dd/MM/yyyy').format(DateTime.parse(task.time!));
  }

  String getTime() {
    return DateFormat.jm().format(DateTime.parse(task.time!));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            title: Text(
              'Task Detail',
              style: font.copyWith(fontSize: 18, color: Colors.black),
            ),
            centerTitle: true,
            backgroundColor: Colors.white,
            bottomOpacity: 0.0,
            elevation: 0.0,
            leading: IconButton(
              color: Colors.black,
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 30, top: 48, right: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    padding: const EdgeInsets.only(bottom: 5),
                    alignment: Alignment.centerLeft,
                    decoration: BoxDecoration(
                        border:
                            Border(bottom: BorderSide(color: grey, width: 2))),
                    height: 32.0,
                    width: 310,
                    child: Column(
                      children: [
                        Text(
                          task.title!,
                          style:
                              font.copyWith(fontSize: 20, color: Colors.black),
                        ),
                      ],
                    )),
                const SizedBox(
                  height: 19,
                ),
                Text(
                  'Schedule detail',
                  style: font.copyWith(fontSize: 18),
                ),
                const SizedBox(
                  height: 12,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 138,
                      height: 40,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: lightGrey,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(9))),
                        child: Row(
                          children: [
                            Image.asset('assets/images/bi_calendar-date.png'),
                            const SizedBox(
                              width: 7,
                            ),
                            Text(
                              getDate(),
                              style: font.copyWith(
                                fontSize: 14,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                        onPressed: () {},
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    SizedBox(
                      width: 138,
                      height: 40,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: lightGrey,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(9))),
                        child: Row(
                          children: [
                            Image.asset('assets/images/carbon_time.png'),
                            const SizedBox(
                              width: 7,
                            ),
                            Text(
                              getTime(),
                              style: font.copyWith(
                                fontSize: 14,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  'Notes',
                  style: font.copyWith(fontSize: 18),
                ),
                const SizedBox(
                  height: 12,
                ),
                Container(
                  padding: const EdgeInsets.only(
                      left: 8, right: 8, top: 8, bottom: 8),
                  height: 158,
                  width: 374,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(9),
                      border: Border.all(color: darkGrey)),
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Text(
                        task.desc!,
                        style: font.copyWith(fontSize: 18, color: Colors.black),
                      )),
                ),
                const SizedBox(
                  height: 60,
                ),
              ],
            ),
          ),
        ));
  }
}
