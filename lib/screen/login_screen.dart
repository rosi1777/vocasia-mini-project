import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:vocasia_mini_project/network/network_url_const.dart';
import 'package:vocasia_mini_project/screen/register_screen.dart';
import 'package:vocasia_mini_project/screen/tabs_screen.dart';
import '../network/network_url_const.dart';

class LoginScreen extends StatefulWidget {
  static const routeName = '/login_screen';
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

enum LoginStatus { notSignIn, signIn }

class _LoginScreenState extends State<LoginScreen> {
  LoginStatus _loginStatus = LoginStatus.notSignIn;

  String? email, password;
  final _key = new GlobalKey<FormState>();

  bool _secureText = true;

  showAlertDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(margin: EdgeInsets.only(left: 5), child: Text("Loading")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  check() {
    final form = _key.currentState!;
    if (form.validate()) {
      form.save();
      signIn();
    }
  }

  signIn() async {
    showAlertDialog(context);
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    // ignore: avoid_init_to_null
    var jsonResponse = null;
    var response = await http.post(Uri.parse(NetworkUrlConst.login),
        body: {"email": email, "password": password});
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse != null) {
        setState(() {
          Navigator.pop(context);
          _loginStatus = LoginStatus.signIn;
        });
        //Sharepreff
        sharedPreferences.setInt("status", jsonResponse['status']);
        sharedPreferences.setString("token", jsonResponse['token']);
        sharedPreferences.setString("id", jsonResponse['data']['id']);
        sharedPreferences.setString("name", jsonResponse['data']['name']);
        sharedPreferences.setString("email", jsonResponse['data']['email']);
        print(response.body);

        Flushbar(
          title: 'Voc.do Apps',
          message: 'Login Successful',
          duration: Duration(seconds: 5),
        ).show(context);
      }
    } else {
      setState(() {
        Navigator.pop(context);
      });
      Flushbar(
        title: 'Voc.do Apps',
        message: 'Please check again. your username or password is wrong.',
        duration: Duration(seconds: 5),
      ).show(context);
      print(response.body);
    }
  }

  var status;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      status = preferences.getInt("status");
      _loginStatus = status == 201 ? LoginStatus.signIn : LoginStatus.notSignIn;
    });
  }

  signOut() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    setState(() {
      // ignore: unnecessary_statements
      preferences.clear();
      preferences.setInt("status", 400);
      _loginStatus = LoginStatus.notSignIn;
      // ignore: deprecated_member_use
      preferences.commit();

      print("Logout Berhasil");
      Flushbar(
        title: 'Voc.do Apps',
        message: 'Logout Successful.',
        duration: Duration(seconds: 5),
      ).show(context);
    });
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    switch (_loginStatus) {
      case LoginStatus.notSignIn:
        var size = MediaQuery.of(context).size;
        return Scaffold(
          backgroundColor: Colors.white,
          body: SafeArea(
              child: Container(
            child: ListView(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: Padding(
                            padding: EdgeInsets.only(top: 120),
                            child: SizedBox(
                              width: 184,
                              height: size.height * 0.15,
                              child: Image.asset(
                                'assets/images/logo.png',
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: Form(
                        key: _key,
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              validator: (e) {
                                if (e != null && e.isEmpty) {
                                  return "Field is required";
                                }
                                if (!RegExp(
                                        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                    .hasMatch(e!)) {
                                  return "Please Enter valid email address";
                                }
                                return null;
                              },
                              onSaved: (e) => email = e,
                              decoration: InputDecoration(
                                labelText: "Email",
                                labelStyle: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey.shade400,
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(color: Colors.grey),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.grey,
                                    )),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            TextFormField(
                              obscureText: _secureText,
                              validator: (e) {
                                if (e != null && e.isEmpty ||
                                    e!.trim().length == 0) {
                                  return "Field is required";
                                }
                                if (e.length < 6) {
                                  return "Minimal password 6 character";
                                }
                              },
                              onSaved: (e) => password = e,
                              decoration: InputDecoration(
                                labelText: "Passowrd",
                                suffixIcon: IconButton(
                                  onPressed: showHide,
                                  icon: Icon(_secureText
                                      ? Icons.visibility_off
                                      : Icons.visibility),
                                  color: Colors.grey,
                                ),
                                labelStyle: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey.shade400,
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(color: Colors.grey),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.grey,
                                    )),
                              ),
                            ),
                            SizedBox(
                              height: 220,
                            ),
                            Container(
                              height: 45,
                              width: 146,
                              // ignore: deprecated_member_use
                              child: FlatButton(
                                onPressed: () {
                                  check();
                                },
                                padding: EdgeInsets.all(0),
                                child: Ink(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(6),
                                      color: Color.fromRGBO(241, 85, 90, 1)),
                                  child: Container(
                                    alignment: Alignment.center,
                                    constraints: BoxConstraints(
                                        maxWidth: double.infinity,
                                        minHeight: 50),
                                    child: Text(
                                      "Login",
                                      style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                  text: 'You dont have an account? ',
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 16,
                                  ),
                                  children: [
                                    TextSpan(
                                      text: 'Register',
                                      style: TextStyle(
                                          color: Colors.red,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    const RegisterScreen(),
                                              ));
                                        },
                                    ),
                                  ]),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(padding: const EdgeInsets.only(bottom: 10))
                  ],
                ),
              ],
            ),
          )),
        );
        // ignore: dead_code
        break;
      case LoginStatus.signIn:
        return TabsScreen(signOut: signOut);
        // ignore: dead_code
        break;
    }
  }
}
