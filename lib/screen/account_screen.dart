// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vocasia_mini_project/theme.dart';

class Account extends StatefulWidget {
  final VoidCallback signOut;
  const Account({Key? key, required this.signOut}) : super(key: key);

  @override
  State<Account> createState() => _AccountState();
}

class _AccountState extends State<Account> {
  signOut() {
    setState(() {
      widget.signOut();
    });
  }

  String? token = "", id = "", name = "", email = "";

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      token = preferences.getString("token");
      name = preferences.getString("name");
    });
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  height: 90,
                  width: 90,
                  child: Stack(
                    fit: StackFit.expand,
                    overflow: Overflow.visible,
                    children: [
                      CircleAvatar(
                        backgroundImage: NetworkImage(
                            "https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cGVyc29ufGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60"),
                      ),
                      Positioned(
                        bottom: 0,
                        right: -10,
                        child: SizedBox(
                          height: 43,
                          width: 43,
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50),
                              side: BorderSide(color: Colors.white, width: 2),
                            ),
                            color: Color.fromRGBO(205, 34, 40, 1),
                            onPressed: () {},
                            child: Image.asset(
                              'assets/images/ic_pencil.png',
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "$name",
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Text(
                        "Edit profile",
                        style: TextStyle(
                          fontSize: 14,
                          color: Color.fromRGBO(121, 121, 121, 1),
                          decoration: TextDecoration.underline,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: grey, width: 2))),
            ),
            SizedBox(
              height: 10,
            ),
            ProfileMenu("Reset Password", "assets/images/ic_tameng.png", () {}),
            ProfileMenu(
                "Delete Account", "assets/images/ic_account2.png", () {}),
            ProfileMenu(
                "Privacy & Policy", "assets/images/ic_privacy.png", () {}),
            ProfileMenu("About", "assets/images/ic_about.png", () {}),
            Center(
              child: ElevatedButton(
                child: Text('Sign Out'),
                onPressed: signOut,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget ProfileMenu(String title, String icon, VoidCallback press) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: TextButton(
            style: TextButton.styleFrom(
              padding: EdgeInsets.all(20),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              backgroundColor: Color(0xFFF5F6F9),
            ),
            onPressed: press,
            child: Row(
              children: [
                Image.asset(
                  icon,
                  color: Color.fromRGBO(58, 58, 58, 1),
                  width: 22.0,
                ),
                SizedBox(width: 20),
                Expanded(
                    child: Text(
                  title,
                  style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(58, 58, 58, 1),
                  ),
                )),
              ],
            ),
          ),
        )
      ],
    );
  }
}
