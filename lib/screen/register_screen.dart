import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:vocasia_mini_project/models/register_response_model.dart';
import 'package:vocasia_mini_project/screen/register_loading_screen.dart';
import 'package:vocasia_mini_project/theme.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);
  static const routeName = '/register_screen';

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  late TextEditingController textControllerNama;
  late TextEditingController textControllerEmail;
  late TextEditingController textControllerPassword;
  late bool passwordVisibility;
  late TextEditingController textControllerConfirmPassword;
  late bool confirmPasswordVisibility;
  final formKey = GlobalKey<FormState>();
  RegisterResponse? registerResponse =
      RegisterResponse(status: 200, error: null, messages: {});

  @override
  void initState() {
    super.initState();
    textControllerNama = TextEditingController();
    textControllerEmail = TextEditingController();
    textControllerPassword = TextEditingController();
    passwordVisibility = false;
    textControllerConfirmPassword = TextEditingController();
    confirmPasswordVisibility = false;
  }

  TextStyle inputTextStyle(textColor) {
    return font.copyWith(color: Color(textColor), fontSize: 16, height: 1.1875);
  }

  OutlineInputBorder inputBorderOutline(borderColor) {
    return OutlineInputBorder(
      borderSide: BorderSide(
        color: Color(borderColor),
        width: 1,
      ),
      borderRadius: BorderRadius.circular(7),
    );
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryHeight = MediaQuery.of(context).size.height;
    final devicePaddingTop = MediaQuery.of(context).padding.top;

    final registerAppBar = AppBar(
      backgroundColor: Colors.white,
      leading: BackButton(
        color: const Color(0xFF454343),
        onPressed: () => Navigator.pop(context),
      ),
      title: Text(
        'Register',
        style: font.copyWith(
          color: const Color(0xFF454343),
          fontSize: 24,
        ),
      ),
      centerTitle: true,
      elevation: 0,
    );

    final bodyHeight = mediaQueryHeight -
        registerAppBar.preferredSize.height -
        devicePaddingTop;
    final emptyBodyHeight = bodyHeight - 370;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: registerAppBar,
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(
                  width: double.infinity,
                  height: emptyBodyHeight * 0.33,
                ),
                Align(
                  alignment: const AlignmentDirectional(0, 0),
                  child: Padding(
                    padding:
                        const EdgeInsetsDirectional.fromSTEB(115, 0, 115, 0),
                    child: Image.asset(
                      'assets/images/logo.png',
                      width: double.infinity,
                      height: 46,
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
                SizedBox(
                  height: emptyBodyHeight * 0.15,
                  width: double.infinity,
                ),
                Form(
                  key: formKey,
                  autovalidateMode: AutovalidateMode.disabled,
                  child: Padding(
                    padding: const EdgeInsetsDirectional.fromSTEB(20, 0, 20, 0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        TextFormField(
                          controller: textControllerNama,
                          obscureText: false,
                          decoration: InputDecoration(
                            hintText: 'Nama',
                            hintStyle: inputTextStyle(0xFF9E9E9E),
                            enabledBorder: inputBorderOutline(
                                textControllerNama.text == ""
                                    ? 0XFF9E9E9E
                                    : 0XFF525252),
                            focusedBorder: inputBorderOutline(
                                textControllerNama.text == ""
                                    ? 0XFF9E9E9E
                                    : 0XFF525252),
                            focusedErrorBorder: inputBorderOutline(0XFFF34040),
                            errorBorder: inputBorderOutline(0XFFF34040),
                            contentPadding:
                                const EdgeInsetsDirectional.fromSTEB(
                                    10, 12, 10, 12),
                          ),
                          style: inputTextStyle(0XFF525252),
                          keyboardType: TextInputType.name,
                          validator: (val) {
                            if (val == null || val == "") {
                              return 'The name field is required';
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 28,
                        ),
                        TextFormField(
                          controller: textControllerEmail,
                          obscureText: false,
                          decoration: InputDecoration(
                            hintText: 'Email',
                            hintStyle: inputTextStyle(0xFF9E9E9E),
                            enabledBorder: inputBorderOutline(
                                textControllerEmail.text == ""
                                    ? 0XFF9E9E9E
                                    : 0XFF525252),
                            focusedBorder: inputBorderOutline(
                                textControllerEmail.text == ""
                                    ? 0XFF9E9E9E
                                    : 0XFF525252),
                            focusedErrorBorder: inputBorderOutline(0XFFF34040),
                            errorBorder: inputBorderOutline(0XFFF34040),
                            contentPadding:
                                const EdgeInsetsDirectional.fromSTEB(
                                    10, 12, 10, 12),
                          ),
                          style: inputTextStyle(0XFF525252),
                          keyboardType: TextInputType.emailAddress,
                          validator: (val) {
                            if (val == null || val == "") {
                              return 'The email field is required';
                            }
                            if (!EmailValidator.validate(val)) {
                              return 'The email field must contain a valid email address';
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 28,
                        ),
                        TextFormField(
                          controller: textControllerPassword,
                          obscureText: !passwordVisibility,
                          decoration: InputDecoration(
                            hintText: 'Password',
                            hintStyle: inputTextStyle(0xFF9E9E9E),
                            enabledBorder: inputBorderOutline(
                                textControllerPassword.text == ""
                                    ? 0XFF9E9E9E
                                    : 0XFF525252),
                            focusedBorder: inputBorderOutline(
                                textControllerPassword.text == ""
                                    ? 0XFF9E9E9E
                                    : 0XFF525252),
                            focusedErrorBorder: inputBorderOutline(0XFFF34040),
                            errorBorder: inputBorderOutline(0XFFF34040),
                            contentPadding:
                                const EdgeInsetsDirectional.fromSTEB(
                                    10, 12, 10, 12),
                            suffixIcon: IconButton(
                              icon: Icon(
                                passwordVisibility
                                    ? Icons.visibility_outlined
                                    : Icons.visibility_off_outlined,
                                color: const Color(0xFF757575),
                                size: 22,
                              ),
                              onPressed: () => setState(
                                () => passwordVisibility = !passwordVisibility,
                              ),
                            ),
                          ),
                          style: inputTextStyle(0XFF525252),
                          keyboardType: TextInputType.visiblePassword,
                          validator: (val) {
                            if (val == null ||
                                !(val.length >= 8 && val.length <= 12)) {
                              return 'Your password should be 8-12 words';
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 28,
                        ),
                        TextFormField(
                          controller: textControllerConfirmPassword,
                          obscureText: !confirmPasswordVisibility,
                          decoration: InputDecoration(
                            hintText: 'Confirm Password',
                            hintStyle: inputTextStyle(0xFF9E9E9E),
                            enabledBorder: inputBorderOutline(
                                textControllerConfirmPassword.text == ""
                                    ? 0XFF9E9E9E
                                    : 0XFF525252),
                            focusedBorder: inputBorderOutline(
                                textControllerConfirmPassword.text == ""
                                    ? 0XFF9E9E9E
                                    : 0XFF525252),
                            focusedErrorBorder: inputBorderOutline(0XFFF34040),
                            errorBorder: inputBorderOutline(0XFFF34040),
                            contentPadding:
                                const EdgeInsetsDirectional.fromSTEB(
                                    10, 12, 10, 12),
                            suffixIcon: IconButton(
                              icon: Icon(
                                confirmPasswordVisibility
                                    ? Icons.visibility_outlined
                                    : Icons.visibility_off_outlined,
                                color: const Color(0xFF757575),
                                size: 22,
                              ),
                              onPressed: () => setState(
                                () => confirmPasswordVisibility =
                                    !confirmPasswordVisibility,
                              ),
                            ),
                          ),
                          style: inputTextStyle(0XFF525252),
                          keyboardType: TextInputType.visiblePassword,
                          validator: (val) {
                            if (val == null ||
                                !(val.length >= 8 && val.length <= 12)) {
                              return 'Your password should be 8-12 words';
                            }
                            if (val != textControllerPassword.text) {
                              return 'The confirm password field does not match the password field';
                            }

                            return null;
                          },
                        ),
                        SizedBox(
                          width: double.infinity,
                          height: emptyBodyHeight * 0.24,
                        ),
                        ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  const Color(0XFFF1555A)),
                              shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(9),
                              ))),
                          onPressed: () async {
                            if (formKey.currentState!.validate()) {
                              registerResponse = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        const LoadingRegisterScreen(),
                                    settings: RouteSettings(
                                      arguments: {
                                        'name': textControllerNama.text,
                                        'email': textControllerEmail.text,
                                        'password': textControllerPassword.text,
                                        'confirmPassword':
                                            textControllerConfirmPassword.text
                                      },
                                    ),
                                  ));
                              if (registerResponse != null) {
                                if (registerResponse!.messages
                                    .containsKey('email')) {
                                  ScaffoldMessenger.of(context)
                                    ..removeCurrentSnackBar()
                                    ..showSnackBar(SnackBar(
                                      content: Text(
                                          registerResponse!.messages['email']),
                                      backgroundColor: const Color(0XFFF34040),
                                      duration: const Duration(seconds: 5),
                                    ));
                                }
                              }
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(
                                10, 10.5, 10, 10.5),
                            child: SizedBox(
                              height: 24,
                              width: 120,
                              child: Text('Signup',
                                  textAlign: TextAlign.center,
                                  style: font.copyWith(
                                    fontSize: 20,
                                  )),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: emptyBodyHeight * 0.28,
                          width: double.infinity,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
