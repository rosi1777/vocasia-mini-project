import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vocasia_mini_project/models/task.dart';
import 'package:vocasia_mini_project/theme.dart';
import 'package:vocasia_mini_project/widgets/list_tasks.dart';
import '../providers/task_provider.dart';

class History extends StatefulWidget {
  const History({Key? key}) : super(key: key);

  @override
  State<History> createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  TaskProvider provider = TaskProvider();
  String? token = "", id = "", name = "", email = "";

  markAsDone(String id) async {
    setState(() {
      provider.markAsDone(id, token!);
    });
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  // Future<Map<String, int>> counter = {} as Future<Map<String, int>>;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      token = preferences.getString("token");
      name = preferences.getString("name");
      id = preferences.getString("id");
    });
  }

  _buildBadgeCategory(String? title, String? iconUrl) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.3,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
              color: Color(0xffDCDCDC), width: 1, style: BorderStyle.solid)),
      child: Row(
        children: [
          ImageIcon(
            AssetImage(iconUrl!),
            size: 15,
            color: Color(0xff9C9C9C),
          ),
          const SizedBox(
            width: 10,
          ),
          Text(
            title!,
            style: font.copyWith(fontSize: 16, color: Color(0xff9C9C9C)),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(24),
          child: Column(
            children: [
              TextField(
                decoration: InputDecoration(
                    icon: ImageIcon(AssetImage("assets/images/ic_search.png")),
                    label: Text(
                      "Search your past schedule...",
                    ),
                    labelStyle:
                        font.copyWith(fontSize: 18, color: Color(0xffBFBFBF)),
                    border: OutlineInputBorder(borderSide: BorderSide.none)),
              ),
              const SizedBox(
                height: 15,
              ),
              Row(
                children: [
                  _buildBadgeCategory('Work', "assets/images/ic_work.png"),
                  const SizedBox(
                    width: 10,
                  ),
                  _buildBadgeCategory(
                      'Individual', "assets/images/ic_personal.png"),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              ListTasks(
                route: "History",
                token: token,
              )
            ],
          ),
        ),
      ),
    );
  }
}
