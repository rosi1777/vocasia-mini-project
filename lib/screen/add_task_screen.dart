import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:vocasia_mini_project/providers/task_provider.dart';
import 'package:vocasia_mini_project/screen/tabs_screen.dart';

import '../theme.dart';

class AddTaskScreen extends StatefulWidget {
  final String token;
  final VoidCallback? signOut;

  static const routeName = '/add_task_screen';
  const AddTaskScreen({Key? key, required this.token, this.signOut})
      : super(key: key);

  @override
  State<AddTaskScreen> createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  final _tittleController = TextEditingController();
  final _notesController = TextEditingController();
  DateTime? date;
  TimeOfDay? time;
  bool isAdded = false;

  String getDate() {
    if (date == null) {
      return 'Set your date';
    } else {
      return DateFormat('dd/MM/yyyy').format(date!);
    }
  }

  String getTime() {
    if (time == null) {
      return 'Select Time';
    } else {
      return time!.format(context);
    }
  }

  Future pickDate(BuildContext context) async {
    final initialDate = DateTime.now();

    final newDate = await showDatePicker(
      context: context,
      initialDate: date ?? initialDate,
      firstDate: DateTime(DateTime.now().year - 5),
      lastDate: DateTime(DateTime.now().year + 5),
    );

    if (newDate == null) return;

    setState(() => date = newDate);
  }

  Future pickTime(BuildContext context) async {
    var initialTime = TimeOfDay.now();
    final newTime = await showTimePicker(
        context: context, initialTime: time ?? initialTime);

    if (newTime == null) return;

    setState(() => time = newTime);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            title: Text(
              'Task Detail',
              style: font.copyWith(fontSize: 18, color: Colors.black),
            ),
            centerTitle: true,
            backgroundColor: Colors.white,
            bottomOpacity: 0.0,
            elevation: 0.0,
            leading: IconButton(
              color: Colors.black,
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 30, top: 48, right: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                      border:
                          Border(bottom: BorderSide(color: grey, width: 2))),
                  height: 32.0,
                  width: 310,
                  child: TextField(
                    keyboardType: TextInputType.text,
                    style: font.copyWith(
                      fontSize: 20,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: const EdgeInsets.only(bottom: 14.0),
                      hintText: 'Tittle',
                      hintStyle: font.copyWith(fontSize: 20, color: grey),
                    ),
                    controller: _tittleController,
                  ),
                ),
                const SizedBox(
                  height: 19,
                ),
                Text(
                  'Schedule detail',
                  style: font.copyWith(fontSize: 18),
                ),
                const SizedBox(
                  height: 12,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 138,
                      height: 40,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: lightGrey,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(9))),
                        child: Row(
                          children: [
                            Image.asset('assets/images/bi_calendar-date.png'),
                            const SizedBox(
                              width: 7,
                            ),
                            Text(
                              getDate(),
                              style: font.copyWith(
                                fontSize: 14,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                        onPressed: () {
                          pickDate(context);
                        },
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    SizedBox(
                      width: 138,
                      height: 40,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: lightGrey,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(9))),
                        child: Row(
                          children: [
                            Image.asset('assets/images/carbon_time.png'),
                            const SizedBox(
                              width: 7,
                            ),
                            Text(
                              getTime(),
                              style: font.copyWith(
                                fontSize: 14,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                        onPressed: () {
                          pickTime(context);
                        },
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  'Notes',
                  style: font.copyWith(fontSize: 18),
                ),
                const SizedBox(
                  height: 12,
                ),
                Container(
                  height: 158,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(9),
                      border: Border.all(color: darkGrey)),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: TextField(
                      maxLines: 10,
                      keyboardType: TextInputType.text,
                      style: font.copyWith(fontSize: 20),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: const EdgeInsets.only(
                            left: 8, right: 8, top: 8, bottom: 8),
                        hintText: 'Type if your task have a note...',
                        hintStyle: font.copyWith(fontSize: 18, color: grey),
                      ),
                      controller: _notesController,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 60,
                ),
                Center(
                  child: SizedBox(
                      width: 140,
                      height: 45,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: red,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(9))),
                        child: Text(
                          'Add',
                          style:
                              font.copyWith(fontSize: 20, color: Colors.white),
                        ),
                        onPressed: () async {
                          await _showMyDialog().whenComplete(() {
                            if (isAdded == true) {
                              Navigator.pop(context);
                            }
                          });
                        },
                      )),
                ),
              ],
            ),
          ),
        ));
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Center(
              child: Text('Created Task',
                  style: font.copyWith(fontSize: 28, color: red)),
            ),
            content: Text(
              'Are you sure want to continue\nadding new task?',
              style: font.copyWith(fontSize: 20),
              textAlign: TextAlign.center,
            ),
            actions: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                      width: 113,
                      height: 44,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(9),
                                side: BorderSide(
                                  width: 1,
                                  color: red,
                                ))),
                        child: Text(
                          'Edit',
                          style: font.copyWith(fontSize: 20, color: red),
                        ),
                        onPressed: () async {
                          Navigator.pop(context);
                        },
                      )),
                  SizedBox(
                      width: 113,
                      height: 44,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: red,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(9))),
                        child: Text(
                          'Continue',
                          style:
                              font.copyWith(fontSize: 20, color: Colors.white),
                        ),
                        onPressed: () async {
                          final timeData =
                              "${date!.toYyMmDd()} ${time!.to24hours()}:00";
                          final tittle = _tittleController.text;
                          final description = _notesController.text;
                          final token = widget.token;
                          isAdded = true;

                          await Provider.of<TaskProvider>(context,
                                  listen: false)
                              .addTask(
                                  token, tittle, description, timeData, context)
                              .whenComplete(() => Navigator.pop(context));
                        },
                      )),
                ],
              )
            ],
            actionsPadding:
                const EdgeInsets.only(bottom: 17, left: 15, right: 15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          );
        });
  }

  @override
  void dispose() {
    _tittleController.dispose();
    _notesController.dispose();
    super.dispose();
  }
}

extension TimeOfDayConverter on TimeOfDay {
  String to24hours() {
    final hour = this.hour.toString().padLeft(2, "0");
    final min = minute.toString().padLeft(2, "0");
    return "$hour:$min";
  }
}

extension DateTimeConverter on DateTime {
  String toYyMmDd() {
    final year = this.year.toString();
    final month = this.month.toString();
    final day = this.day.toString();
    return "$year-$month-$day";
  }
}
