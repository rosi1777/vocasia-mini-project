import 'dart:async';
import 'package:flutter/material.dart';
import 'package:vocasia_mini_project/models/register_response_model.dart';
import 'package:vocasia_mini_project/screen/login_screen.dart';
import 'package:vocasia_mini_project/theme.dart';

class LoadingRegisterScreen extends StatefulWidget {
  const LoadingRegisterScreen({Key? key}) : super(key: key);
  static const routeName = '/loading_register_screen';

  @override
  _LoadingRegisterScreenState createState() => _LoadingRegisterScreenState();
}

class _LoadingRegisterScreenState extends State<LoadingRegisterScreen> {
  @override
  Widget build(BuildContext context) {
    late String name;
    late String email;
    late String password;
    late String confirmPassword;

    var args =
        ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
    if (args.containsKey('name')) {
      name = args['name'];
    }
    if (args.containsKey('email')) {
      email = args['email'];
    }
    if (args.containsKey('password')) {
      password = args['password'];
    }
    if (args.containsKey('confirmPassword')) {
      confirmPassword = args['confirmPassword'];
    }

    Timer successNavigator() {
      const duration = Duration(seconds: 5);
      return Timer(duration, () {
        Navigator.pushReplacementNamed(context, LoginScreen.routeName);
      });
    }

    Timer errorNavigator(RegisterResponse? registerResponse) {
      const duration = Duration(seconds: 5);
      return Timer(duration, () {
        Navigator.pop(context, registerResponse);
      });
    }

    Column successRegisterScreen() {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/ep_success-filled.png',
            width: double.infinity,
            height: 175.88,
            fit: BoxFit.contain,
          ),
          const SizedBox(
            height: 32.56,
          ),
          Text(
            "SUCCESS",
            style: font.copyWith(fontSize: 30),
          )
        ],
      );
    }

    SizedBox loadingScreen() {
      return const SizedBox(
        width: double.infinity,
        height: double.infinity,
      );
    }

    return Scaffold(
      body: SafeArea(
          child: FutureBuilder<RegisterResponse>(
              future: RegisterResponse.postToAPI(
                  name, email, password, confirmPassword),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data?.status == 201 &&
                      snapshot.data?.error == null) {
                    successNavigator();
                    return successRegisterScreen();
                  } else {
                    errorNavigator(snapshot.data);
                    return loadingScreen();
                  }
                } else {
                  return loadingScreen();
                }
              })),
    );
  }
}
