import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vocasia_mini_project/widgets/filter_dialog.dart';
import 'package:vocasia_mini_project/widgets/list_badges.dart';
import 'package:vocasia_mini_project/widgets/list_tasks.dart';
import '../theme.dart';
import '../widgets/custom_appbar.dart';
import '../widgets/filter_button.dart';
import 'add_task_screen.dart';

enum Status { done, overdue, todo }

class HomeScreen extends StatefulWidget {
  static bool? isViewDoneOnly;
  static bool? isViewOverdueOnly;
  final VoidCallback? signOut;
  HomeScreen(this.signOut);
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Status? status;
  ScrollController sc = ScrollController();
  bool? viewDone;
  bool? viewOverdue;
  bool isIndividual = false;
  bool isWork = false;
  String? token = "", id = "", name = "", email = "";
  bool isFab = false;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      token = preferences.getString('token');
      name = preferences.getString("name");
      id = preferences.getString("id");
    });
  }

  @override
  void initState() {
    getPref();
    // sc.addListener(() {
    //   if (sc.offset > 50) {
    //     setState(() {
    //       isFab = true;
    //     });
    //   } else {
    //     setState(() {
    //       isFab = false;
    //     });
    //   }
    // });
    setState(() {
      viewDone = HomeScreen.isViewDoneOnly ?? false;
      viewOverdue = HomeScreen.isViewOverdueOnly ?? false;
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    sc.dispose();
  }

  _buildFiterButton() {
    return InkWell(
      onTap: () {
        setState(() {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return FilterDialog(widget.signOut);
              });
        });
      },
      child: Container(
          padding: const EdgeInsets.symmetric(vertical: 7, horizontal: 5),
          decoration: BoxDecoration(
              color: const Color(0xffFCFCFC),
              borderRadius: BorderRadius.circular(10),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, blurRadius: 0.25, offset: Offset(0, 0))
              ]),
          width: 80,
          height: 30,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const ImageIcon(
                AssetImage('assets/images/ic_filter.png'),
                size: 14,
              ),
              Text(
                'FILTER',
                style: font.copyWith(fontSize: 12, fontWeight: FontWeight.w500),
              )
            ],
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    print(viewDone);
    return Scaffold(
        body: SafeArea(
            child: SingleChildScrollView(
          controller: sc,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomAppBar(
                    username: name,
                    date: DateFormat.MMMEd().add_y().format(DateTime.now()),
                    imgUrl:
                        'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cGVyc29ufGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60'),
                const SizedBox(
                  height: 30,
                ),
                Center(
                  child: Text('Today',
                      style: font.copyWith(
                          color: const Color(0xff121212), fontSize: 22)),
                ),
                const SizedBox(
                  height: 30,
                ),
                ListBadges(
                  token: token,
                ),
                const SizedBox(
                  height: 20,
                ),
                FilterButton(widget.signOut),
                SizedBox(
                  height: 20,
                ),
                ListTasks(
                  route: "Today",
                  token: token,
                  isViewDoneOnly: viewDone ?? false,
                  isViewOverdueOnly: viewOverdue ?? false,
                  signOut: widget.signOut,
                )
              ],
            ),
          ),
        )),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: AnimatedContainer(
          duration: Duration(milliseconds: 200),
          curve: Curves.linear,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              gradient: const LinearGradient(colors: [
                Color(0xffE9464C),
                Color(0xffC83338),
                Color(0xffE9464C),
              ])),
          child: FloatingActionButton.extended(
              backgroundColor: Colors.transparent,
              elevation: 0,
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AddTaskScreen(
                              token: token!,
                              signOut: widget.signOut,
                            ))).then((value) => setState(() {}));
              },
              icon: const Icon(
                Icons.add,
                color: Colors.white,
              ),
              label: Center(
                child: Text(
                  'Add Task',
                  style: font.copyWith(color: Colors.white, fontSize: 18),
                ),
              )),
        ));
  }
}
