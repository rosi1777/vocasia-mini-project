import 'package:flutter/material.dart';
import 'package:vocasia_mini_project/screen/login_screen.dart';
import 'package:vocasia_mini_project/providers/preference_provider.dart';
import 'package:vocasia_mini_project/theme.dart';
import 'package:provider/provider.dart';

class OnBoardingScreen extends StatelessWidget {
  static const routeName = '/onboarding_screen';
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final emptySpace = screenHeight - 396;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: emptySpace * 0.35,
              ),
              Image.asset('assets/images/Ilustrasi.png'),
              SizedBox(
                height: emptySpace * 0.25,
              ),
              Text(
                'Managed Your\nSchedule Easily With\nVocasia',
                style: font.copyWith(
                  fontSize: 36,
                ),
              ),
              SizedBox(
                height: emptySpace * 0.35,
              ),
              SizedBox(
                width: 157,
                height: 52,
                child: Consumer<PreferenceProvider>(
                    builder: (context, provider, child) {
                  return ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: red,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(31))),
                    child: Text(
                      'Get Started',
                      style: font.copyWith(fontSize: 20, color: Colors.white),
                    ),
                    onPressed: () async {
                      bool value = true;
                      provider.setOnBoardingPreference(value);
                      Navigator.pushReplacementNamed(
                          context, LoginScreen.routeName);
                    },
                  );
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
