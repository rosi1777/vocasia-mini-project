import 'dart:async';

import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:vocasia_mini_project/screen/login_screen.dart';
import 'package:vocasia_mini_project/screen/onboarding_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  static const routeName = '/splash_screen';
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late bool isViewed;

  @override
  void initState() {
    getOnBoardingPreference().whenComplete(() async {
      Timer(
          const Duration(seconds: 3),
          () => isViewed
              ? Navigator.pushReplacementNamed(context, LoginScreen.routeName)
              : Navigator.pushReplacementNamed(
                  context, OnBoardingScreen.routeName));
    });
    super.initState();
  }

  Future getOnBoardingPreference() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var isSplashViewed = sharedPreferences.getBool('splash');
    setState(() {
      isViewed = isSplashViewed ?? false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              DelayedDisplay(
                  delay: const Duration(seconds: 1),
                  child: Image.asset('assets/images/logo.png')),
              const SizedBox(
                height: 7,
              ),
              DelayedDisplay(
                  delay: const Duration(seconds: 2),
                  child: Image.asset('assets/images/tittle.png'))
            ],
          ),
        ),
      ),
    );
  }
}
