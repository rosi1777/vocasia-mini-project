import 'package:flutter/material.dart';
import 'package:vocasia_mini_project/screen/home_screen.dart';
import 'account_screen.dart';
import 'history_screen.dart';

class TabsScreen extends StatefulWidget {
  final VoidCallback? signOut;
  static String routeName = '/tab_screen';
  TabsScreen({this.signOut});
  @override
  State<TabsScreen> createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  VoidCallback? signOut;
  List _pages = [];
  @override
  void initState() {
    _pages = [
      HomeScreen(widget.signOut),
      History(),
      Account(signOut: widget.signOut as VoidCallback)
    ];
    super.initState();
  }

  int _selectedPage = 0;
  void _selectPage(int index) {
    setState(() {
      _selectedPage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pages[_selectedPage] as Widget,
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        currentIndex: _selectedPage,
        items: const [
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage('assets/images/ic_home.png'),
              ),
              label: 'Home'),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage('assets/images/ic_history.png'),
              ),
              label: 'History'),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage('assets/images/ic_account.png'),
              ),
              label: 'Account')
        ],
      ),
    );
  }
}
