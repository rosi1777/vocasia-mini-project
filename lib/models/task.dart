class Task {
  String? id;
  String? userId;
  String? title;
  String? desc;
  String? status;
  String? time;

  Task({
    required this.id,
    required this.userId,
    required this.title,
    required this.desc,
    required this.status,
    required this.time,
  });

  Task.fromJson(json) {
    id = json['id'];
    userId = json['user_id'];
    title = json['tittle'];
    desc = json['description'];
    status = json['status'];
    time = json['time'];
  }
}
