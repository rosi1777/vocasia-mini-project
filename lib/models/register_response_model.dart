import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:vocasia_mini_project/network/network_url_const.dart';

class RegisterResponse {
  int status;
  dynamic error;
  Map<String, dynamic> messages;

  RegisterResponse(
      {required this.status, required this.error, required this.messages});

  factory RegisterResponse.registerPost(Map<String, dynamic> object) {
    return RegisterResponse(
        status: object['status'],
        error: object['error'],
        messages: object['messages']);
  }

  static Future<RegisterResponse> postToAPI(String name, String email,
      String password, String confirmPassword) async {
    Uri apiURL = Uri.parse(NetworkUrlConst.register);

    var apiResult = await http.post(apiURL, body: {
      "name": name,
      "email": email,
      "password": password,
      "confirmpassword": confirmPassword
    });
    var jsonObject = jsonDecode(apiResult.body);

    return RegisterResponse.registerPost(jsonObject);
  }
}
