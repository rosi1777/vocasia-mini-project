import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

class ApiService {
  final String baseUrl;
  ApiService({required this.baseUrl});

  Future<dynamic> addTask(
      {required String token,
      required String tittle,
      String? description,
      required String time}) async {
    var url = Uri.parse('$baseUrl/create');
    var body = {'tittle': tittle, 'description': description, 'time': time};
    final response = await http.post(
      url,
      headers: {HttpHeaders.authorizationHeader: 'Bearer $token'},
      body: body,
    );

    if (response.statusCode == 201) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to add task');
    }
  }

  Future<dynamic> updateTask(
      {required String token,
      required String id,
      required String tittle,
      String? description,
      required String time}) async {
    var url = Uri.parse('$baseUrl/update/$id');
    var body = {'tittle': tittle, 'description': description, 'time': time};
    final response = await http.put(
      url,
      headers: {HttpHeaders.authorizationHeader: 'Bearer $token'},
      body: body,
    );

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to add task');
    }
  }
}
