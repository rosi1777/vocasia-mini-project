import 'package:shared_preferences/shared_preferences.dart';

class PreferencesHelper {
  final Future<SharedPreferences> sharedPreferences;

  PreferencesHelper({required this.sharedPreferences});

  static const onBoardingPreferenceKey = 'splash';
  static const tokenPreferenceKey = 'token';

  Future<String?> get token async {
    final prefs = await sharedPreferences;
    return prefs.getString(tokenPreferenceKey) ?? "";
  }

  void settokenPreference(String value) async {
    final prefs = await sharedPreferences;
    prefs.setString(onBoardingPreferenceKey, value);
  }

  Future<bool> get isOnBoardingViewed async {
    final prefs = await sharedPreferences;
    return prefs.getBool(onBoardingPreferenceKey) ?? false;
  }

  void setOnBoardingPreference(bool value) async {
    final prefs = await sharedPreferences;
    prefs.setBool(onBoardingPreferenceKey, value);
  }
}
