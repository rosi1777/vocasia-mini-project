import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../theme.dart';

class CustomAppBar extends StatefulWidget {
  String? username;
  final String? date;
  final String? imgUrl;

  CustomAppBar({
    required this.username,
    @required this.date,
    @required this.imgUrl,
  });

  @override
  State<CustomAppBar> createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.username!,
              style: font.copyWith(
                  color: Colors.black,
                  fontSize: 22,
                  fontWeight: FontWeight.w500),
            ),
            Text(
              widget.date!,
              style:
                  font.copyWith(color: const Color(0xff8a8a8a), fontSize: 14),
            )
          ],
        ),
        ClipRRect(
          borderRadius: BorderRadius.circular(100),
          child: Container(
            width: 48,
            height: 48,
            child: Image.network(
              widget.imgUrl!,
              fit: BoxFit.cover,
            ),
          ),
        )
      ],
    );
  }
}
