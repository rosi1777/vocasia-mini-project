import 'package:flutter/material.dart';
import 'package:vocasia_mini_project/screen/home_screen.dart';
import 'package:vocasia_mini_project/screen/tabs_screen.dart';
import 'package:vocasia_mini_project/theme.dart';

class FilterDialog extends StatefulWidget {
  final VoidCallback? signOut;
  FilterDialog(this.signOut);
  @override
  State<FilterDialog> createState() => _FilterDialogState();
}

enum Status { done, overdue, todo }
enum Category { work, individual }

class _FilterDialogState extends State<FilterDialog> {
  Status? status = Status.todo;
  Category? category;
  bool isIndividual = false;
  bool isWork = false;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 30, horizontal: 10),
        width: MediaQuery.of(context).size.width * 0.5,
        height: MediaQuery.of(context).size.height * 0.55,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 15,
              ),
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  'Status',
                  style: font.copyWith(fontSize: 22),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Radio(
                    toggleable: true,
                    activeColor: Color(0xffD6BB2F),
                    value: Status.overdue,
                    groupValue: status,
                    onChanged: (Status? value) {
                      setState(() {
                        status = value;
                      });
                    }),
                Text(
                  'Overdue',
                  style: font.copyWith(fontSize: 18),
                )
              ],
            ),
            Row(
              children: [
                Radio(
                    toggleable: true,
                    activeColor: Color(0xff30C047),
                    value: Status.done,
                    groupValue: status,
                    onChanged: (Status? value) {
                      setState(() {
                        status = value;
                      });
                    }),
                Text(
                  'Done',
                  style: font.copyWith(fontSize: 18),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 15,
              ),
              child: Text(
                'Categories',
                style: font.copyWith(fontSize: 22),
              ),
            ),
            Row(
              children: [
                Checkbox(
                  value: isWork,
                  onChanged: (bool? value) {
                    setState(() {
                      isWork = !isWork;
                    });
                  },
                  tristate: true,
                ),
                Text('Work', style: font.copyWith(fontSize: 18))
              ],
            ),
            Row(
              children: [
                Checkbox(
                  value: isIndividual,
                  onChanged: (bool? value) {
                    setState(() {
                      isIndividual = !isIndividual;
                    });
                  },
                  tristate: true,
                ),
                Text('Individual', style: font.copyWith(fontSize: 18))
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Center(
              child: InkWell(
                onTap: () {
                  setState(() {
                    switch (status) {
                      case Status.done:
                        HomeScreen.isViewDoneOnly = true;
                        HomeScreen.isViewOverdueOnly = false;
                        break;
                      case Status.overdue:
                        HomeScreen.isViewDoneOnly = false;
                        HomeScreen.isViewOverdueOnly = true;
                        break;
                      default:
                    }
                  });
                  Navigator.of(context).pop();
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => TabsScreen(
                                signOut: widget.signOut,
                              )));
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: Color(0xffF1555A),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  width: MediaQuery.of(context).size.width * 0.25,
                  padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                  child: Text(
                    'Filter',
                    textAlign: TextAlign.center,
                    style: font.copyWith(fontSize: 20, color: Colors.white),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
