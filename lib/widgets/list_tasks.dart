import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vocasia_mini_project/network/network_url_const.dart';
import 'package:vocasia_mini_project/screen/tabs_screen.dart';
import 'package:vocasia_mini_project/screen/task_detail_screen.dart';
import 'package:vocasia_mini_project/screen/update_task_screen.dart';
import '../providers/task_provider.dart';
import '../models/task.dart';
import 'package:http/http.dart' as http;

import '../theme.dart';

class ListTasks extends StatefulWidget {
  String? route;
  String? token;
  bool? isViewDoneOnly;
  bool? isViewOverdueOnly;
  final VoidCallback? signOut;
  ListTasks(
      {required this.route,
      required this.token,
      this.isViewDoneOnly = false,
      this.isViewOverdueOnly = false,
      this.signOut});

  @override
  State<ListTasks> createState() => _ListTasksState();
}

class _ListTasksState extends State<ListTasks> {
  String token = "";
  TaskProvider taskProvider = TaskProvider();

  @override
  void initState() {
    getTokenPreference();
    super.initState();
  }

  Future getTokenPreference() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var getToken = sharedPreferences.getString('token');
    token = getToken!;
  }

  Widget _buildTaskCard({required Task task, String? status}) {
    return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, TaskDetailScreen.routeName,
              arguments: task);
        },
        child: Container(
          width: double.infinity,
          margin: const EdgeInsets.symmetric(vertical: 5),
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: const Color(0xffF4F4F4),
              border: Border(
                  left: BorderSide(
                      width: 5,
                      color: status! == "1"
                          ? const Color(0xff3EB150)
                          : const Color(0xffD1B934)))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.4,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FittedBox(
                      child: Text(
                        task.title!,
                        style: font.copyWith(
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                            color: const Color(0xff121212)),
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        const ImageIcon(
                          AssetImage(
                              'assets/images/ic_sharp-supervisor-account.png'),
                          size: 15,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          DateFormat.jm().format(DateTime.parse(task.time!)),
                          style: font.copyWith(color: const Color(0xff595959)),
                        )
                      ],
                    )
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.4,
                child: widget.route == "Today"
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          status == "0"
                              ? IconButton(
                                  onPressed: () {
                                    // Mark as Done Task
                                    setState(() {
                                      http.put(
                                          Uri.parse(
                                              "${NetworkUrlConst.markAsDone}/${task.id}"),
                                          headers: {
                                            HttpHeaders.authorizationHeader:
                                                "Bearer $token"
                                          }).whenComplete(() => status = "1");
                                    });
                                  },
                                  icon: const ImageIcon(
                                    AssetImage('assets/images/ic_done.png'),
                                    size: 20,
                                    color: Color(0xffE05156),
                                  ))
                              : SizedBox(),
                          IconButton(
                              onPressed: () {
                                // Function to Edit Task\
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => UpdateTaskScreen(
                                            task: task,
                                            signOut: widget.signOut,
                                          )),
                                ).then((value) => setState(() {}));
                              },
                              icon: const ImageIcon(
                                AssetImage('assets/images/ic_edit.png'),
                                size: 20,
                                color: Color(0xffE05156),
                              )),
                          IconButton(
                              onPressed: () {
                                // Delete Task
                                setState(() {
                                  setState(() {
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return Dialog(
                                            child: Container(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 20, horizontal: 20),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          12)),
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.7,
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.3,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    'Delete Task',
                                                    style: font.copyWith(
                                                        fontSize: 28,
                                                        color:
                                                            Color(0xffFF3F3F)),
                                                  ),
                                                  SizedBox(
                                                    height: 15,
                                                  ),
                                                  Text(
                                                    'Are you sure want to delete your task?',
                                                    textAlign: TextAlign.center,
                                                    style: font.copyWith(
                                                      fontSize: 20,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 30,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: [
                                                      InkWell(
                                                        onTap: () {
                                                          Navigator.pop(
                                                              context);
                                                        },
                                                        child: Container(
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width *
                                                              0.2,
                                                          height: 30,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          8),
                                                              border: Border.all(
                                                                  color: Color(
                                                                      0xffF1555A),
                                                                  width: 1.5)),
                                                          child: Text(
                                                            'Back',
                                                            textAlign: TextAlign
                                                                .center,
                                                            style:
                                                                font.copyWith(
                                                                    fontSize:
                                                                        20),
                                                          ),
                                                        ),
                                                      ),
                                                      InkWell(
                                                        onTap: () {
                                                          setState(() {
                                                            http.delete(
                                                                Uri.parse(
                                                                    'https://apitodolist.nouky.xyz/todolist/delete/${task.id}'),
                                                                headers: {
                                                                  HttpHeaders
                                                                          .authorizationHeader:
                                                                      'Bearer $token'
                                                                });
                                                          });
                                                          Navigator.pushReplacement(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) =>
                                                                      TabsScreen(
                                                                          signOut:
                                                                              widget.signOut)));
                                                          // Navigator.pop(
                                                          //     context);
                                                        },
                                                        child: Container(
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width *
                                                              0.2,
                                                          height: 30,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          8),
                                                              color: Color(
                                                                  0xffF1555A),
                                                              border: Border.all(
                                                                  color: Color(
                                                                      0xffF1555A),
                                                                  width: 1.5)),
                                                          child: Text(
                                                            'Delete',
                                                            textAlign: TextAlign
                                                                .center,
                                                            style: font.copyWith(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 20),
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  )
                                                ],
                                              ),
                                            ),
                                          );
                                        });
                                  });
                                });
                              },
                              icon: const ImageIcon(
                                AssetImage('assets/images/ic_delete.png'),
                                size: 20,
                                color: Color(0xffE05156),
                              ))
                        ],
                      )
                    : Align(
                        alignment: Alignment.centerRight,
                        child: IconButton(
                            onPressed: () {
                              // Navigate to Task Details
                              Navigator.pushNamed(
                                  context, TaskDetailScreen.routeName,
                                  arguments: task);
                            },
                            icon: Icon(Icons.chevron_right))),
              )
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Task>>(
      future: widget.route == "Today"
          ? taskProvider.getTodayTask(token)
          : taskProvider.getAllTask(token),
      builder: (context, snap) {
        if (snap.hasData) {
          List<Task>? doneTask =
              snap.data!.where((item) => item.status == "1").toList();
          List<Task>? overdueTask =
              snap.data!.where((item) => item.status == "0").toList();
          List<Task>? task;
          if (widget.isViewDoneOnly!) {
            task = List<Task>.from(doneTask);
          } else if (widget.isViewOverdueOnly!) {
            task = List<Task>.from(overdueTask);
          } else {
            task = List<Task>.from(overdueTask)..addAll(doneTask);
          }
          print("List task : ${widget.isViewDoneOnly}");
          return Column(
            children: task.map((item) {
              return _buildTaskCard(
                task: item,
                status: item.status,
              );
            }).toList(),
          );
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
