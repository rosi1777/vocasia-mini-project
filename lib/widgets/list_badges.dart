import 'package:flutter/material.dart';
import 'package:vocasia_mini_project/providers/task_provider.dart';

import '../theme.dart';

class ListBadges extends StatelessWidget {
  String? token;
  ListBadges({Key? key, required this.token}) : super(key: key);

  TaskProvider taskProvider = TaskProvider();

  _buildBadgeContainer(
      {int? amount, String? desc, List<Color>? colors, BuildContext? context}) {
    return Container(
      width: MediaQuery.of(context!).size.width * 0.25,
      height: 64,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Colors.amber,
          gradient: LinearGradient(
            colors: colors!,
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
          )),
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            amount.toString(),
            style: font.copyWith(color: const Color(0xffFCFCFC), fontSize: 20),
          ),
          Text(desc!,
              style:
                  font.copyWith(color: const Color(0xff494949), fontSize: 16))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: taskProvider.getCounter(token!),
      builder: (context, snap) {
        var counter;
        if (snap.hasData) {
          counter = snap.data;
        } else {
          counter = {};
        }
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buildBadgeContainer(
                amount: counter['todo'] == null
                    ? 0
                    : counter['todo'] + counter['overdue'],
                desc: 'To Do',
                colors: badgeColors[0]['colors'] as List<Color>,
                context: context),
            _buildBadgeContainer(
                amount: counter['overdue'] ?? 0,
                desc: 'Overdue',
                colors: badgeColors[1]['colors'] as List<Color>,
                context: context),
            _buildBadgeContainer(
                amount: counter['done'] ?? 0,
                desc: 'Done',
                colors: badgeColors[2]['colors'] as List<Color>,
                context: context),
          ],
        );
      },
    );
  }
}
