import 'package:flutter/material.dart';
import '../theme.dart';
import 'filter_dialog.dart';

class FilterButton extends StatelessWidget {
  final VoidCallback? signOut;

  const FilterButton(this.signOut);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return FilterDialog(signOut);
            });
      },
      child: Container(
          padding: const EdgeInsets.symmetric(vertical: 7, horizontal: 5),
          decoration: BoxDecoration(
              color: const Color(0xffFCFCFC),
              borderRadius: BorderRadius.circular(10),
              boxShadow: const [
                BoxShadow(
                    color: Colors.black, blurRadius: 0.25, offset: Offset(0, 0))
              ]),
          width: 80,
          height: 30,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const ImageIcon(
                AssetImage('assets/images/ic_filter.png'),
                size: 14,
              ),
              Text(
                'FILTER',
                style: font.copyWith(fontSize: 12, fontWeight: FontWeight.w500),
              )
            ],
          )),
    );
  }
}
